
#define LED_PIN 13
#define RATE 38400

char val;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(RATE);
  Serial2.begin(RATE);
}

void loop() {
  if (Serial.available()) {
    val = Serial.read();
    Serial2.print(val);
  }
 
  if (Serial2.available()) {
    val = Serial2.read();
    Serial.print(val);
  }
}
