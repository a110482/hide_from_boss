#ifndef JDY_SLAVE_CPP
#define JDY_SLAVE_CPP
#include "JDY_SLAVE.h"
#include "SoftwareSerial.h"


JDY_SLAVE_STATUS JDY_SLAVE::status() {
	int status = digitalRead(this->bush_pin);
	return status == HIGH ? CONNECTED : DISCONNECTED;
}

void JDY_SLAVE::setup(uint8_t bush_pin, SoftwareSerial* serial) {
	this->serial = serial;
	this->bush_pin = bush_pin;
	pinMode(bush_pin, INPUT_PULLUP);
	serial->print("at+reset\r\n");
	delay(2000);
	serial->print("at+role0\r\n");
}

void JDY_SLAVE::send(String msg) {
	this->serial->print(msg);
}





#endif // !JDY_SLAVE_CPP