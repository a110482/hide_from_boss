#include "Arduino.h"
#include "SoftwareSerial.h"
#include "JDY_SLAVE.h"

#define LED_PIN 13

#define BLE_STATUS_PIN 10
#define INFRARED_PIN 8


SoftwareSerial mySerial(7, 6);
JDY_SLAVE* slave;


void setup() {
	Serial.begin(9600);
	mySerial.begin(9600);
	slave = new JDY_SLAVE();
	slave->setup(BLE_STATUS_PIN, &mySerial);


	digitalWrite(LED_PIN, HIGH);
	delay(3000);
	digitalWrite(LED_PIN, LOW);
	Serial.println("init");
}

String msg = "";
unsigned long sr505_active_time = 0;
bool is_reset = false;

void loop() {
	if (mySerial.available()) {
		msg = mySerial.readString();
		Serial.print(msg);
	}
	if (Serial.available()) {
		msg = Serial.readString();
		mySerial.print(msg);
	}

	// 判斷 HC-SR505 紅外線模組訊號
	if (digitalRead(INFRARED_PIN) == HIGH) {
		sr505_active_time = millis();
		if (is_reset) {
			is_reset = false;
			slave->send("alert");
			Serial.println("alert");
		}
	}
	else if (!is_reset && millis() - sr505_active_time > 1000){
		is_reset = true;
		slave->send("reset");
		Serial.println("reset");
	}


	if (digitalRead(INFRARED_PIN) == HIGH) {
		digitalWrite(LED_PIN, HIGH);
	}
	else {
		digitalWrite(LED_PIN, LOW);
	}
}
