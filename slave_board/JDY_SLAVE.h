#ifndef JDY_SLAVE_H
#define JDY_SLAVE_H

#include "Arduino.h"
#include "SoftwareSerial.h"

enum JDY_SLAVE_STATUS {
	CONNECTED,
	DISCONNECTED
};


class JDY_SLAVE {
private:
	uint8_t bush_pin = 0;
	SoftwareSerial* serial;
public:
	JDY_SLAVE_STATUS status();
	void setup(uint8_t bush_pin, SoftwareSerial* serial);
	void send(String msg);
};



#endif // !JDY_SLAVE_H
