// for board Pro Micro
#include "LED_STATUS.h"
#include "JDY_MASTER.h"
#include "SoftwareSerial.h"


#define LED_PIN 13
#define BLE_STATUS_PIN 5

JDY_MASTER* master;
SoftwareSerial mySerial(7, 6);

void setup() {
    Serial.begin(9600);
    mySerial.begin(9600);
    master = new JDY_MASTER();
    master->serial = &mySerial;
    pinMode(LED_PIN, OUTPUT);
    pinMode(BLE_STATUS_PIN, INPUT_PULLUP);

    digitalWrite(LED_PIN, HIGH);
    delay(300);
    digitalWrite(LED_PIN, LOW);
    delay(300);
    digitalWrite(LED_PIN, HIGH);
    delay(300);
    digitalWrite(LED_PIN, LOW);
    Serial.println("hello");
}

String serial_read = "";
void loop() {
    serial_read = "";
    if (mySerial.available()) {
        serial_read = mySerial.readString();
        Serial.println(serial_read);
        if (serial_read == "alert") {
            Serial.println("revcive alert!!!");
           
        }
        else if (serial_read == "reset") {
            Serial.println("sensor reset...");
        }
        else {
            Serial.print("other msg: ");
            Serial.println(serial_read);
        }
    }
    
    if (digitalRead(BLE_STATUS_PIN) == LOW) {
        master->reset_process();
    }

    master->loop(serial_read);
}
