#include "JDY_MASTER.h"


void JDY_MASTER::send_at_cmd(String cmd) {
    cmd += "\r\n";
	serial->print("at+" + cmd);
	Serial.print("at+" + cmd);
};


void JDY_MASTER::test() {
    this->send_at_cmd("at+role1");
}

void JDY_MASTER::loop(String receive_feedback)
{
	this->receive_feedback = receive_feedback;
	switch (this->main_status)
	{
	case SET_ROLE:
		this->set_role_precess();
		break;
	case SEARCH:
		this->search_precess();
		break;
	case CONNECT:
		this->connect_process();
		break;
	}
}

void JDY_MASTER::reset_process()
{
	if (reset) { return; }
	reset = true;
	main_status = SET_ROLE;
	sub_status = PREPARE;
}


// ==== at command process ====
void JDY_MASTER::set_role_precess() {
	switch (this->sub_status)
	{
	case PREPARE:
		this->send_at_cmd("role1");
		this->sub_status = DOING;
		break;
	case DOING:
		if (this->receive_feedback.indexOf("OK") >= 0) {
			this->sub_status = COMPLETED;
		}
		break;
	case COMPLETED:
		this->main_status = SEARCH;
		this->sub_status = PREPARE;
		timestamp = millis();
		break;
	}
}

void JDY_MASTER::search_precess() {
	switch (this->sub_status)
	{
	case PREPARE:
		if (millis() - timestamp < cool_down) {
			break;
		}
		this->settings.slave_device_id = "0";
		this->send_at_cmd("inq");
		this->sub_status = DOING;
		break;
	case DOING:
		if (this->receive_feedback == "") {
			break;
		}
		if (this->receive_feedback.indexOf(this->settings.slave_name) >= 0) {
			int index = this->receive_feedback.indexOf("=");
			this->settings.slave_device_id = this->receive_feedback.substring(index-1, index);
		}
		if (this->receive_feedback.indexOf("STOP") >= 0) {
			this->sub_status = COMPLETED;
		}
		break;
	case COMPLETED:
		timestamp = millis();
		if (this->settings.slave_device_id == "0") {
			this->sub_status = PREPARE;
		} else {
			this->main_status = CONNECT;
			this->sub_status = PREPARE;
		}
		break;
	}
}

void JDY_MASTER::connect_process() {
	switch (this->sub_status)
	{
	case PREPARE:
		if (millis() - timestamp < cool_down) {
			break;
		}
		this->send_at_cmd("conn" + this->settings.slave_device_id);
		this->sub_status = DOING;
		break;
	case DOING:
		// +CONNECTED
		if (this->receive_feedback == "") {
			break;
		}
		if (this->receive_feedback.indexOf("CONNECTED") >= 0) {
			this->sub_status = COMPLETED;
			reset = false;
		} else {
			this->main_status = SEARCH;
			this->sub_status = PREPARE;
		}
		break;
	case COMPLETED:
		if (this->receive_feedback == "") {
			break;
		}
		if (this->receive_feedback.indexOf("+DISCONNECTED") >= 0) {
			this->main_status = SEARCH;
			this->sub_status = PREPARE;
		}
		// static status
		break;
	}
}
