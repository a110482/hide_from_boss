#ifndef JDY_MASTER_H
#define JDY_MASTER_H
#include "Arduino.h"
#include "SoftwareSerial.h"


enum JDY_MASTER_STATUS
{
    SET_ROLE,
    SEARCH,
    CONNECT,
};
enum JDY_MASTER_SUB_STATUS
{
    PREPARE,
    DOING,
    COMPLETED
};

struct JDY_MASTER_SETTINGS
{
    String slave_name = "JDY-18";
    String slave_device_id = "0";
};


class JDY_MASTER
{
private:
    void send_at_cmd(String cmd);
    String receive_feedback = "";
    unsigned long timestamp = 0;
    const int cool_down = 1000;
    bool reset;
    
    void set_role_precess();
    void search_precess();
    void connect_process();
public:
    void test();
    void loop(String receive_feedback = "");
    void reset_process();
    SoftwareSerial *serial;

    JDY_MASTER_SETTINGS settings = JDY_MASTER_SETTINGS();
    JDY_MASTER_STATUS main_status = SET_ROLE;
    JDY_MASTER_SUB_STATUS sub_status = PREPARE;
};

#endif //JDY_MASTER_H
