// HIDDEX_CONTROLLER.h

#ifndef _HIDDEX_CONTROLLER_h
#define _HIDDEX_CONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
	#include "Keyboard.h"
#else
	#include "WProgram.h"
#endif

class HIDDEX_CONTROLLER {
private:
	uint8_t safe_pin;
public:
	HIDDEX_CONTROLLER(uint8_t safe_pin);
};



#endif

