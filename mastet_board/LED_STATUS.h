#ifndef LED_STATUS_H
#define LED_STATUS_H

class LED_STATUS
{
private:
    int led_pin;
    unsigned long time_stamp;
    bool led_status;
    int led_blink_amount;
public:
    LED_STATUS(int led_pin);
    void loop();
    void setup();
    void light_led(int amount);
};




#endif //LED_STATUS