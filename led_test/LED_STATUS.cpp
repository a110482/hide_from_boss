#include "LED_STATUS.h"
#include "arduino.h"

LED_STATUS::LED_STATUS(int led_pin) {
    this->led_pin = led_pin;
    this->time_stamp = 0;
    this->led_status = false;
    this->led_blink_amount = 0;
};

void LED_STATUS::loop() {
    if (this->led_blink_amount == 0) {
        return;
    }
    if ((millis() - this->time_stamp) < 200) {
        return;
    }
    if (led_status) {
        digitalWrite(this->led_pin, LOW);
        this->led_blink_amount -= 1;
        this->led_status = false;
    } else {
        digitalWrite(this->led_pin, HIGH);
        this->led_status = true;
    }
    this->time_stamp = millis();
};

void LED_STATUS::light_led(int amount) {
    this->led_blink_amount = amount;
};