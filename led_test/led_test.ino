#include <LED_STATUS.h>

#define LED_PIN 13

LED_STATUS led = LED_STATUS(LED_PIN);

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available()) {
    Serial.read();
    led.light_led(2);
  }
  led.loop();
}
